const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());
// 配置解析表单数据的中间件 注意：这个中间件，只能解析application/x-www-form-urlencoded 格式的表单
app.use(express.urlencoded({ extended: false }));

// 配置解析JSON格式的中间件
app.use(express.json());

// 一定要在路由之前封装res.cc函数
app.use((req, res, next) => {
  res.cc = (code = 400, err) => {
    res.send({
      code: code,
      msg: err instanceof Error ? err.message : err,
    });
  };
  next();
});

// 一定要在路由之前配置解析 Token的中间件
const expressJWT = require("express-jwt");
const config = require("./config.js");

// 使用 .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证
app.use(
  expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\//] })
);

// 导入并使用用户路由模块
const userRouter = require("./router/user.js");
const userInfoRouter = require("./router/userInfo.js");
const workRouter = require("./router/work.js");
app.use("/api", userRouter);
app.use("/my", userInfoRouter);
app.use("/work", workRouter);

// 定义错误级别的中间件
app.use((err, req, res, next) => {
  // 捕获身份认证失败的错误
  if (err.name === "UnauthorizedError") return res.cc(401, "身份认证失败");
  res.cc(500, err);
});

// 启动服务器
app.listen(3001, () => {
  console.log("服务器启动成功,http://127.0.0.1:3001");
});
