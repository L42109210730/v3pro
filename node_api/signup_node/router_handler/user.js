const db = require("../db/index.js");
const jwt = require("jsonwebtoken");
const config = require("../config.js");

//登录的处理函数
exports.login = function (req, res) {
  // 获取客户端提交到服务器的用户信息
  const userInfo = req.body;
  // 定义SQL语句
  const sqlStr = "select * from users where username=? and password=?";
  db.query(sqlStr, [userInfo.username, userInfo.password], (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    if (results.length !== 1) {
      return res.cc(400, "登录失败");
    }
    // TODO: 在服务端生成Token的字符串
    const user = { ...results[0], password: "" };
    // 对用户的信息进行加密，生成Token字符串
    const token = jwt.sign(user, config.jwtSecretKey, {
      expiresIn: config.expiresIn,
    });
    // 调用res.send()将Token响应给客户端
    res.send({ code: 200, msg: "登录成功", token: token });
  });
};
