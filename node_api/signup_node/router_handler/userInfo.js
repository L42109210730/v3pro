const db = require("../db/index.js");

// exports.getUserName = function (req, res) {
//   // 获取客户端提交到服务器的用户信息
//   const userInfo = req.query;
//   const sqlStr = "select * from users where username=?";
//   db.query(sqlStr, [userInfo.username], (err, results) => {
//     if (err) return res.cc(500, "数据库查询错误");
//     if (results.length !== 1) {
//       return res.cc(400, "用户名或Token错误");
//     }
//     const userList = {
//       username: results[0].username,
//       password: results[0].password,
//       name: results[0].name,
//       phone: results[0].phone,
//       role: {},
//       userType: results[0].userType,
//       school: results[0].school,
//       class: results[0].class,
//     };
//     const roleStr = "select *  from role where userType=?";
//     db.query(roleStr, [userList.userType], (err, dataArr) => {
//       userList.role = dataArr[0].roleName;
//       // 调用res.send()将Token响应给客户端
//       res.send({ code: 200, msg: "获取成功", data: userList });
//     });
//   });
// };
exports.getUserName = (req, res) => {
  // 获取客户端提交到服务器的用户信息
  const userInfo = req.query;
  const sqlStr = "select * from users where username=?";
  db.query(sqlStr, [userInfo.username], (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    if (results.length !== 1) {
      return res.cc(400, "用户名或Token错误");
    }
    const userList = {
      userType: results[0].userType,
    };
    res.send({ code: 200, msg: "获取成功", data: userList });
  });
};

// 获取用户列表信息
exports.getUserList = (req, res) => {
  // 获取客户端传递的分页参数，默认为第一页，每页5条数据
  const page = req.query.page || 1;
  const pageSize = req.query.pageSize || 5;
  // 计算 OFFSET 值
  const offset = (page - 1) * pageSize;
  const roleName = req.query.roleName;
  const school = req.query.school;
  // 构建 SQL 查询语句，带有 LIMIT 和 OFFSET 子句
  let sqlStr = `SELECT * FROM users 
              JOIN role ON users.userType = role.userType`;
  if (roleName) {
    sqlStr += ` WHERE role.roleName='${roleName}'`;
  }
  if (school) {
    if (roleName) {
      sqlStr += ` AND users.school='${school}'`;
    } else {
      sqlStr += ` WHERE users.school='${school}'`;
    }
  }
  sqlStr += ` LIMIT ${pageSize} OFFSET ${offset}`;
  db.query(sqlStr, (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    const data = results.map((item) => {
      return {
        roleName: item.roleName,
        school: item.school,
        name: item.name,
        username: item.username,
        password: item.password,
        phone: item.phone,
        userType: item.userType,
      };
    });
    const count = `select count(*) as total from users `;
    db.query(count, (err, counts) => {
      if (err) return res.cc(500, "数据库查询错误");
      const total = counts[0].total;
      return res.send({
        code: 200,
        msg: "查询成功",
        data,
        page,
        pageSize,
        total,
      });
    });
  });
};

// 删除用户信息
exports.deleteUser = (req, res) => {
  const username = req.query.username;
  const sqlStr = "delete from users where username=?";
  db.query(sqlStr, [username], (err, data) => {
    if (err) return res.cc(500, "数据库查询错误");
    if (data.affectedRows > 0) return res.cc(200, "删除成功");
    return res.cc(500, "删除失败");
  });
};

// 新增用户信息
exports.addUser = (req, res) => {
  const userInfo = req.body;
  const sqlStr = "insert into users set ?";
  db.query(sqlStr, userInfo, (err, data) => {
    if (err) return res.cc(500, "数据库查询错误");
    if (data.affectedRows > 0) return res.cc(200, "新增成功");
    return res.cc(500, "新增失败");
  });
};

// 编辑用户信息
exports.editUser = (req, res) => {
  const userInfo = req.body;
  const sqlStr = "update users set ? where username=?";
  db.query(sqlStr, [userInfo, userInfo.username], (err, data) => {
    if (err) return res.cc(500, "数据库查询错误");
    if (data.affectedRows > 0) return res.cc(200, "编辑成功");
    return res.cc(500, "编辑失败");
  });
};
