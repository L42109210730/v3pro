const db = require("../db/index.js");

// 获取作品列表
exports.getworkList = (req, res) => {
  const page = req.query.page || 1;
  const pageSize = req.query.pageSize || 5;
  // 计算 OFFSET 值
  const offset = (page - 1) * pageSize;
  const workname = req.query.workname;
  const name = req.query.name;
  const school = req.query.school;
  const className = req.query.className;
  const state = req.query.state;
  let sqlStr = `select * from users 
  join work on users.username=work.username
  join class ON work.classId = class.classId
  join workstate ON work.state = workstate.state
  
   WHERE (${workname} IS NULL OR work.workname = ${workname})
    AND (${name} IS NULL OR users.name = ${name})
    AND (${school} IS NULL OR users.school = ${school})
    AND (${className} IS NULL OR class.className = ${className})
    AND (${state} IS NULL OR workstate.state = ${state})
   LIMIT ${pageSize} OFFSET ${offset}`;
  db.query(sqlStr, (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    const data = results.map((item) => {
      return {
        workId: item.workId,
        workname: item.workname,
        name: item.name,
        school: item.school,
        className: item.className,
        state: item.state,
        submittime: item.submittime,
        score: item.score,
      };
    });
    const count = `select count(*) as total from work `;
    db.query(count, (err, counts) => {
      if (err) return res.cc(500, "数据库查询错误");
      const total = counts[0].total;
      return res.send({
        code: 200,
        msg: "查询成功",
        data,
        page,
        pageSize,
        total,
      });
    });
  });
};

// 新增作品
exports.addwork = (req, res) => {
  const workInfo = req.body;
  const sqlStr = "insert into work set ?";
  db.query(sqlStr, workInfo, (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    return res.send({ code: 200, msg: "新增成功" });
  });
};
// 编辑作品
exports.editwork = (req, res) => {
  const workInfo = req.body;
  const sqlStr = "update work set ? where workId=?";
  db.query(sqlStr, [workInfo, workInfo.workId], (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    return res.send({ code: 200, msg: "编辑成功" });
  });
};

// 删除作品
exports.deletework = (req, res) => {
  const workId = req.query.workId;
  const sqlStr = "delete from work where workId=?";
  db.query(sqlStr, workId, (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    return res.send({ code: 200, msg: "删除成功" });
  });
};
// 打分
exports.editscore = (req, res) => {
  const workInfo = req.body;
  const sqlStr = "update work set score=? where workId=?";
  db.query(sqlStr, [workInfo.score, workInfo.workId], (err, results) => {
    if (err) return res.cc(500, "数据库查询错误");
    return res.send({ code: 200, msg: "已评审" });
  });
};
