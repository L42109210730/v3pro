const joi = require("joi");
// alphanuw()值只能是包含a-zA-Z0-9的字符串
// required()值是必须项，不能为undefiend
// pattern（正则表达式）值必须符合正则表达式的规则
const username = joi.string().alphanum().min(5).max(12).required();
const password = joi
  .string()
  .pattern(/^[\S]{6,12}$/)
  .required();
// 登录表单的验证规则对象
exports.reg_login_schema = {
  // 表示需要对req.body 中的数据进行验证
  body: {
    username,
    password,
  },
};
