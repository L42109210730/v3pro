const express = require("express");
const router = express.Router();

// 导入作品信息的处理函数模块
const workInfo = require("../router_handler/work");

// 查询作品列表
router.get("/workList", workInfo.getworkList);

//新增作品信息
router.post("/addwork", workInfo.addwork);

//更新作品信息
router.post("/editwork", workInfo.editwork);

//打分
router.post("/editscore", workInfo.editscore);

module.exports = router;
