import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  // server: {
  //   proxy: {
  //     "/api": {
  //       target: "http://127.0.0.1:3001",
  //       // 是否重写路径，将请求路径中的 /api 前缀去除
  //       rewrite: (path) => path.replace(/^\/api/, ""),
  //       // headers: {
  //       //   Authorization: `Bearer ${token}`,
  //       // },
  //       // 是否将请求头中的主机头 host 改为目标地址的主机头，默认为 true
  //       changeOrigin: true,
  //       // 是否开启 websocket 代理，默认为 false
  //       ws: true,
  //     },
  //   },
  // },
});
