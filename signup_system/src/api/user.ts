// 封装所有和用户相关的接口函数
import instance from "@/utils/request";

export const login = (data: any) => {
  const { username, password } = data;
  return instance({
    url: "/api/login",
    method: "POST",
    data: { username, password },
  });
};
export const getLoginInfo = (data: any) => {
  return instance({
    url: `/my/getUserInfo?username=${data}`,
    method: "GET",
  });
};

export const getUserList = (
  page: number = 1,
  pageSize: number = 5,
  userType: number,
  school: string
) => {
  return instance({
    url: `/my/userList?page=${page}&pageSize=${pageSize}&
    userType=${userType}&school=${school}`,
    method: "GET",
  });
};

export const addUser = (data: any) => {
  return instance({
    url: "/my/addUser",
    method: "POST",
    data,
  });
};
export const editUser = (data: any) => {
  return instance({
    url: "/my/editUser",
    method: "POST",
    data,
  });
};

export const delUser = (data: any) => {
  return instance({
    url: `/my/deleteUser?id=${data}`,
    method: "GET",
  });
};
