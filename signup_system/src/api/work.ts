import instance from "@/utils/request";

export const getworkInfo = (data: any) => {
  return instance({
    url: `/work/getworkInfo?workname=${data}`,
    method: "GET",
  });
};

export const getworkList = (page: number = 1, pageSize: number = 5) => {
  return instance({
    url: `/work/workList?page=${page}&pageSize=${pageSize}`,
    method: "GET",
  });
};

export const addwork = (data: any) => {
  return instance({
    url: "/work/addwork",
    method: "POST",
    data,
  });
};
export const editwork = (data: any) => {
  return instance({
    url: "/work/editwork",
    method: "POST",
    data,
  });
};

export const delwork = (data: any) => {
  return instance({
    url: `/work/deletework?id=${data}`,
    method: "GET",
  });
};

export const editScore = (data: any) => {
  return instance({
    url: "/work/editscore",
    method: "POST",
    data,
  });
};
