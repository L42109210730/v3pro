import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "首页",
      component: Home,
    },
    {
      path: "/home",
      redirect: "/",
    },
    {
      path: "/about",
      name: "比赛指南",
      component: () => import("../views/About.vue"),
    },
    {
      path: "/sign",
      name: "我要报名",
      component: () => import("@/views/SignUp.vue"),
      children: [
        {
          path: "",
          name: "填写作品信息",
          component: () => import("@/views/MySign/WorkInfo.vue"),
        },
        {
          path: "/upload",
          name: "上传参赛作品",
          component: () => import("@/views//MySign/UploadWork.vue"),
        },
      ],
    },
    {
      path: "/expert",
      name: "专家评审",
      component: () => import("@/views/Expert.vue"),
    },
    {
      path: "/admin",
      name: "活动",
      meta: { title: "活动" },
      component: () => import("@/views/Admin.vue"),
      children: [
        {
          path: "",
          name: "账号管理",
          meta: { title: "账号管理" },
          component: () => import("@/views/MyAdmin/Account.vue"),
        },
        {
          path: "/work",
          name: "作品管理",
          meta: { title: "作品管理" },
          component: () => import("@/views/MyAdmin/Work.vue"),
        },
        {
          path: "/comment",
          name: "评价管理",
          meta: { title: "评价管理" },
          component: () => import("@/views/MyAdmin/Comment.vue"),
        },
      ],
    },
  ],
});

export default router;
