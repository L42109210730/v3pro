import axios from "axios";
import { ElMessage } from "element-plus";
import { userStore } from "@/stores/user";

const instance = axios.create({
  baseURL: " http://127.0.0.1:3001",
  timeout: 3000,
});
instance.interceptors.request.use(
  function (config) {
    const useStore = userStore();
    const token = useStore.token;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } //获取用户信息时携带token
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    // 统一错误提示
    ElMessage({
      type: "warning",
      message: error.response.message,
    });

    return Promise.reject(error);
  }
);

export default instance;
