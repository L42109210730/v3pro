import { ref } from "vue";
import { defineStore } from "pinia";

export const userStore = defineStore(
  "user",
  () => {
    // 定义 token 变量，并使用 ref 包装
    const token = ref();
    // 用户登录的数据
    const userData = ref({
      username: "",
      password: "",
    });
    const userType = ref();

    // 定义设置 token 的方法
    const setToken = (newToken: any) => {
      token.value = newToken;
    };
    const setData = (data: any) => {
      userData.value = data;
    };
    const setType = (data: any) => {
      userType.value = data;
    };
    // 定义清除 token 的方法
    const removeToken = () => {
      token.value = "";
    };
    const removeUser = () => {
      userData.value = {
        username: "",
        password: "",
      };
    };
    const removeType = () => {
      userType.value = 0;
    };
    // 返回 store 对象，包含 token、setToken、removeToken
    return {
      token,
      userData,
      userType,
      setToken,
      setData,
      setType,
      removeToken,
      removeUser,
      removeType,
    };
  },
  {
    persist: true,
  }
);
